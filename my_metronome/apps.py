from django.apps import AppConfig


class MyMetronomeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'my_metronome'
